import './style.scss'
import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import StartScreen from './components/StartScreen'
import TriviaScreen from './components/TriviaScreen'
import ResultScreen from './components/ResultScreen'
import { initStorage } from './storage/results-data'

Vue.use(VueRouter);
initStorage();

const routes = [
  {path: '/', component: StartScreen },
  {path: '/game', component: TriviaScreen },
  {path: '/results', component: ResultScreen }
]

const router = new VueRouter({ routes })

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')


