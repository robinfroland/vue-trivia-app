const KEY = 'results-data';
const initalData = {
    score: 0,
    results: []
};

// Usually (key, value), but only one key in storage
export const setResults = (data) => 
    localStorage.setItem(KEY, JSON.stringify(data));

export const getResults = () => {
    console.log(localStorage.getItem(KEY))
    return JSON.parse(localStorage.getItem(KEY))};

export const initStorage = () => setResults(initalData);

