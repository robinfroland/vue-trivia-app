const QUESTIONS_URL = "https://opentdb.com/api.php?amount=10&category=15&difficulty=medium";

export const getQuestions = () => {
    return fetch(QUESTIONS_URL)
        .then(response => response.json())
        .then(response => response.results)
    };


